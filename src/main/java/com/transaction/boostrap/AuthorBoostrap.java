package com.transaction.boostrap;

import com.transaction.domain.Author;
import com.transaction.repositories.AuthorRepository;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AuthorBoostrap implements ApplicationListener<ContextRefreshedEvent> {
    private final AuthorRepository authorRepository;

    public AuthorBoostrap(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        authorRepository.saveAll(getAuthor());
    }

    private List<Author> getAuthor(){
        List<Author> authors = new ArrayList<>();
        authors.add(new Author("alex"));
        authors.add(new Author("james"));
        authors.add(new Author("bobby"));

        return authors;
    }
}
