package com.transaction.repositories;

import com.transaction.domain.Author;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Optional;

public interface AuthorRepository extends CrudRepository<Author, Long> {
    @Override
    Optional<Author> findById(Long id);


    //native query
    @Query(value = "SELECT author FROM Author author", nativeQuery = true)
    Collection<Author> findAllUsers();
}
