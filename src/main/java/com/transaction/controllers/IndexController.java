package com.transaction.controllers;

import com.transaction.HibernateUtils;
import com.transaction.domain.Author;
import com.transaction.repositories.AuthorRepository;
import com.transaction.services.AuthorService;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Optional;

@Controller
public class IndexController {
    private AuthorRepository authorRepository;
    private final AuthorService authorService;

    public IndexController(AuthorService authorService, AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
        this.authorService = authorService;
    }

    @RequestMapping({"", "/", "index"})
    public String getIndexPage(){
        Optional<Author> authorOptional = authorRepository.findById(1L);
        System.out.println("author with id = 1 has name: " + authorOptional.get().getName());
        return "index";
    }

    public void updateAuthorName(Long id, String name){
        authorService.updateAuthorName(id, name);
    }

    public void updateAuthorNameByTransaction(Long id, String name){
        authorService.updateAuthorWithRollbackCustom(id, name);
    }


    //HQL
    public void updateByHQL(Long id, String name){
        try (Session session = HibernateUtils.getSessionFactory().openSession();) {
            // Begin a unit of work
            session.beginTransaction();

            // HQL
            String hql = "UPDATE Author SET name = :name WHERE id = :id";
            Query query = session.createQuery(hql);
            query.setParameter("name", name);
            query.setParameter("id", id);
            int affectedRows = query.executeUpdate();

            session.getTransaction().commit();
        }
    }
}
