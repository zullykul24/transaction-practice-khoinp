package com.transaction;

import com.transaction.controllers.IndexController;
import com.transaction.domain.Author;
import com.transaction.repositories.AuthorRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Collection;
import java.util.List;

@SpringBootApplication
public class TransactionPracticeApplication {

    public static void main(String[] args) {

        ApplicationContext ctx = SpringApplication.run(TransactionPracticeApplication.class, args);
        IndexController controller = ctx.getBean("indexController", IndexController.class);

        controller.updateAuthorName(1L, "Nam");
        controller.updateAuthorNameByTransaction(2L, "Transaction");
        controller.updateByHQL(3L, "HQL");
        //first author's name change from alex to Nam
        //second author's name change from james to Transaction
        //third author's name change from bobby to HQL


        System.out.println("List users:");
        AuthorRepository authorRepository = ctx.getBean(AuthorRepository.class);
        Collection<Author> list = authorRepository.findAllUsers();
        for(Author i: list){
            System.out.println(i.toString());
        }
    }

}
