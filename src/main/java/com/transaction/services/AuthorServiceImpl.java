package com.transaction.services;

import com.transaction.domain.Author;
import com.transaction.repositories.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

@Service
public class AuthorServiceImpl implements AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    @Override
    @Transactional
    public void updateAuthorName(Long id, String name) {
        Author author = authorRepository.findById(id).get();
        author.setName(name);
    }

    @Override
    @Transactional(rollbackFor = Exception.class, noRollbackFor = EntityNotFoundException.class)
    public void updateAuthorWithRollbackCustom(Long id, String name) {
        Author author = authorRepository.findById(id).orElse(null);
        author.setName(name);
    }

}
