package com.transaction.services;

import org.springframework.stereotype.Service;

public interface AuthorService {
    void updateAuthorName(Long id, String name);
    void updateAuthorWithRollbackCustom(Long id, String name);
}
